#!/usr/bin/env python3
#
# MediaWiki New Errors Checker
# ============================
#
# Keeps the `mediawiki-new-errors` dashboard clean on logstash by:
#
# 1. Fetching the json describing the existing dashboard from OpenSearch
# 2. Finding any dashboard filters that contain Phab-style task IDs (matching /T\d+/)
# 3. Searching Phabricator for all task IDs
# 4. Creating a new json dashboard, removing task filter from the dashboard
#    unless their status is "open"
# 5. Unless this is a `--dry-run`, re-writing the old dashboard with the new
#
# This uses the following APIs:
#
# - Phabricator conduit's maniphest.search API: <https://phabricator.wikimedia.org/conduit/method/maniphest.search/>
# - OpenSearch's saved object API: <https://www.elastic.co/guide/en/kibana/current/saved-objects-api.html>
#
# Copyright © 2023 Tyler Cipriani
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html
#
import argparse
import copy
from functools import lru_cache
import getpass
import json
import logging
import os
import re
import shutil
from tempfile import NamedTemporaryFile

from phabricator import Phabricator
import requests
from requests.auth import HTTPBasicAuth


STORAGE_PATH = os.path.dirname(os.path.abspath(__file__))
DEFAULT_DASHBOARD = '0a9ecdc0-b6dc-11e8-9d8f-dbc23b470465'  # mediawiki-new-errors


def yn_prompt(prompt):
    """
    Yes/no prompt
    """
    while True:
        response = input(prompt)
        if response.lower() in ("y", "n"):
            return response.lower() == "y"
        print("Please enter 'y' or 'n'.")


class LogstashDashboard():
    def __init__(self, dashboard_id):
        self.url = 'https://logstash.wikimedia.org/api/saved_objects'
        self.dashboard_id = dashboard_id
        self.cache_file = os.path.join(STORAGE_PATH, f'{self.dashboard_id}.json')

    @property
    @lru_cache(maxsize=None)
    def auth(self):
        user = os.environ.get('LOGSTASH_USER')
        if user is None:
            user = input('LDAP username for logstash: ')
        password = os.environ.get('LOGSTASH_PASS')
        if password is None:
            password = getpass.getpass(prompt='LDAP password for logstash: ')
        return HTTPBasicAuth(user, password)

    @property
    @lru_cache(maxsize=None)
    def bugs(self):
        self.dashboard_json = self._load_dashboard()
        bugs = set()

        dashboard_embedded_json = self.dashboard_json['attributes']['kibanaSavedObjectMeta']['searchSourceJSON']

        filters = json.loads(dashboard_embedded_json)
        for filter in filters['filter']:
            # This is where the "label" is stored in a logstash filter
            alias = filter['meta'].get('alias')
            if alias is None:
                continue
            task_id = self._get_task_id(alias)
            if task_id is None:
                continue
            bugs.add(task_id)
        return bugs

    def remove_tasks(self, bugs):
        new_dashboard = self._generate_new_dashboard(
            filter_bugs=bugs,
            old_dashboard=self._load_dashboard()
        )
        headers = {
            'osd-xsrf': 'true',
        }

        r = requests.post(
            os.path.join(self.url, '_import?overwrite=true'),
            headers=headers,
            files={'file': ('file.ndjson', json.dumps(new_dashboard))},
            auth=self.auth
        )
        logging.debug(r.content)
        r.raise_for_status()

    @lru_cache(maxsize=None)
    def _load_dashboard(self):
        """
        Load the dashboard json from cache or fetch logstash
        """
        if self._keep_cache():
            with open(self.cache_file) as f:
                return json.load(f)

        return json.loads(self._fetch_dashboard())

    def _keep_cache(self):
        logging.debug('Checking cache file: %s', self.cache_file)
        # If the cache doesn't exist, we need to download a new one
        if not os.path.exists(self.cache_file):
            return False

        return not yn_prompt('Overwrite with fresh dashboard data (y/n)? ')

    def _fetch_dashboard(self):
        """
        Retrieve the json document for a dashboard from logstash
        (and cache it on disk)
        """
        headers = {
            'osd-xsrf': 'true',
            'Content-Type': 'application/json',
        }
        data = {'objects': [{
            'type': 'dashboard',
            'id': self.dashboard_id
        }]}

        r = requests.post(
            os.path.join(self.url, '_export'),
            headers=headers,
            auth=self.auth,
            data=json.dumps(data)
        )
        r.raise_for_status()
        logging.debug('Fetched dashboard')

        # We only care about the first line
        dashboard_json = r.text.splitlines()[0]

        # Cache it
        with NamedTemporaryFile(
            prefix='errorcheck.XXXXX',
            mode='w',
            delete=False
        ) as temp_file:
            temp_file_path = temp_file.name
            logging.debug('Saving to "%s"', temp_file_path)
            temp_file.write(dashboard_json)

        shutil.move(temp_file_path, self.cache_file)
        return dashboard_json

    def _generate_new_dashboard(self, filter_bugs, old_dashboard):
        new_dashboard = copy.deepcopy(old_dashboard)

        old_filters = json.loads(
            new_dashboard['attributes']['kibanaSavedObjectMeta']['searchSourceJSON']
        )
        new_filters = {'filter': []}
        for filter in old_filters['filter']:
            # This is where the "label" is stored in a logstash filter
            alias = filter['meta'].get('alias')
            if alias is None:
                continue
            task_id = self._get_task_id(alias)
            if task_id in filter_bugs:
                logging.debug(f'Removing {task_id}')
                continue
            new_filters['filter'].append(filter)
        new_dashboard['attributes']['kibanaSavedObjectMeta']['searchSourceJSON'] = json.dumps(new_filters)
        return new_dashboard

    @staticmethod
    def _get_task_id(label):
        """
        Return a task ID if a label contains one
        otherwise return None
        """
        task_id = None
        task_line = re.match(r'T\d+', label)
        if task_line:
            task_id = task_line[0]
        return task_id


def maniphest_search(phab, bugs, tasks=None, after=None):
    """
    Recursively search maniphest for tasks based on a list of bugs
    """
    query = {
        'constraints': {
            'ids': bugs,
        }
    }
    if after is not None:
        query['after'] = after
    if tasks is None:
        tasks = []
    out = phab.maniphest.search(**query)
    tasks += out['data']
    if out.get('cursor') and out['cursor'].get('after'):
        logging.debug('Phab search more results: %s', after)
        return maniphest_search(phab, bugs, tasks, out['cursor']['after'])
    else:
        return tasks


def filter_open_tasks(bugs):
    """
    filters a list of bugs so only the unopened ones remain

    TODO: do we want this to do something fancy with duplicates?
    """
    phab = Phabricator()
    # phabricator expects integers
    bug_ids = [int(x[1:]) for x in bugs]
    nonopen_tasks = set()
    tasks = maniphest_search(phab, bug_ids)
    for task in tasks:
        if task['fields']['status']['value'] == 'open':
            continue
        nonopen_tasks.add('T{}'.format(task['id']))
    return nonopen_tasks


def setup_logging(verbose):
    level = logging.INFO
    if verbose:
        level = logging.DEBUG

    logging.basicConfig(level=level)


def parse_args():
    args = argparse.ArgumentParser()
    args.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        help='Enable verbose output'
    )
    args.add_argument(
        '-i',
        '--id',
        default=DEFAULT_DASHBOARD,
        help=f'ID of Dashboard (default: {DEFAULT_DASHBOARD})'
    )
    args.add_argument(
        '-d',
        '--dry-run',
        action='store_true',
        help="Don't delete, just do a dry run"
    )
    return args.parse_args()


def main():
    args = parse_args()
    setup_logging(args.verbose)
    lsd = LogstashDashboard(args.id)
    closed_tasks = filter_open_tasks(lsd.bugs)
    logging.info('Removing the following tasks\n{}'.format(
        '\n'.join([
            f'https://phabricator.wikimedia.org/{t}' for t in closed_tasks
        ])
    ))
    if not args.dry_run:
        yn_prompt('Remove non-open tasks from dashboard (y/n)? ')
        lsd.remove_tasks(closed_tasks)


if __name__ == '__main__':
    main()
